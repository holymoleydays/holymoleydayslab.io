"""
This module provides a function to generate iCalendar (.ics) files
for holidays of various countries and subdivisions.
"""

from collections import OrderedDict
from datetime import datetime
import holidays
from ics import Calendar, Event
from utils import (
    get_country_name,
    get_subdiv_name,
    list_all_countries_with_translations,
    list_countries_and_subdivs,
)



current_year = datetime.now().year
YEARS = range(current_year - 1, current_year + 11)


def modify_ics_header(file_path, region):
    # From: https://stackoverflow.com/questions/17152251/specifying-name-description-and-refresh-interval-in-ical-ics-format
    new_header = f'''BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Holy Moly Days!//python-holidays, ics-py, and pycountry//NONSGML Holiday Calendar
URL:http://holymolydays.gitlab.io/{file_path}
NAME:{region} | Holy Moly Days!
X-WR-CALNAME:{region} | Holy Moly Days!
DESCRIPTION:Official holidays for {region} thanks to python-holidays
X-WR-CALDESC:Official holidays for {region} thanks to python-holidays
REFRESH-INTERVAL;VALUE=DURATION:PT24H
X-PUBLISHED-TTL:PT24H
CALSCALE:GREGORIAN
METHOD:PUBLISH
'''

    with open(file_path, 'r') as file:
        lines = file.readlines()
        file.close()

    for i, line in enumerate(lines):
        if line.startswith('BEGIN:VEVENT'):
            break

    with open(file_path, 'w') as file:
        file.writelines(new_header + ''.join(lines[i:]))
        file.close()


def save_holidays_to_ics(holidays_dict, filename):
    """
    Saves the given holidays in the given dictionary to an iCalendar (.ics) file.

    Parameters:
    holidays_dict (dict): A dictionary containing the holidays to save.
                          The keys are datetime.date objects representing the
                          holiday dates, and the values are strings representing
                          the holiday names.
    filename (str): The name of the output file.

    Returns:
    None
    """
    # Create a new calendar
    cal = Calendar()
    events_ordered_set = OrderedDict()

    # Loop through each holiday and create an event in the calendar
    for date, name in sorted(holidays_dict.items()):
        event = Event()
        event.name = name
        event.begin = date
        event.make_all_day()
        events_ordered_set[date] = event

    # Add the events to the calendar in the order they were added
    cal.events = events_ordered_set.values()

    # Save the calendar to a file
    with open(filename, 'w') as file:
        file.writelines(cal.serialize_iter())


def save_all_holidays_to_ics():
    """
    Generates iCalendar (.ics) files for all holidays of various countries
    and subdivisions, and saves them to files.

    Parameters:
    None

    Returns:
    None
    """
    countries_with_translations = list_all_countries_with_translations()
    for country, subdiv in list_countries_and_subdivs():
        holidays_dict = holidays.country_holidays(country, subdiv=subdiv, years=YEARS)
        filename = f'{country}-{subdiv}.ics' if subdiv else f'{country}.ics'
        print(f'Saving: {filename}')
        save_holidays_to_ics(holidays_dict, filename)
        country_name = get_country_name(country)
        subdiv_name = get_subdiv_name(country, subdiv)
        region = f'{subdiv_name}' if subdiv_name else f'{country_name}'
        modify_ics_header(filename, region)

        # Also export *-{lang}.ics files for countries with translations
        if country in countries_with_translations:
            langs = countries_with_translations[country]
            for lang in langs:
                holidays_dict = holidays.country_holidays(
                    country, subdiv=subdiv, years=YEARS, language=lang
                )
                filename = f'{country}-{subdiv}-{lang}.ics' if subdiv else f'{country}-{lang}.ics'
                print(f'-> Saving translation: {filename}')
                save_holidays_to_ics(holidays_dict, filename)
                modify_ics_header(filename, region)


if __name__ == "__main__":
    save_all_holidays_to_ics()
