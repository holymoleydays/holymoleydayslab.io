"""
Utility functions for working with countries and languages.
"""

import os
import re
from typing import Dict, List, Optional

import holidays
import pycountry


def list_countries_and_subdivs() -> List[List[Optional[str]]]:
    """
    List all supported countries and subdivisions.

    Returns:
        List[List[Optional[str]]]: A list of country codes and their subdivisions
        (if any), sorted by country code.
    """
    for country_code, subdivs in sorted(holidays.list_supported_countries(include_aliases=False).items()):
        yield [country_code, None]
        if subdivs is not None:
            for subdiv in subdivs:
                yield [country_code, subdiv]


def list_all_countries_with_translations() -> Dict[str, List[Optional[str]]]:
    """
    Recursively search for all .po files in python-holidays's locale dir.
    Assume the filename is the country code the parent's parent dir is the language.

    The structure looks like this:

    holidays/locale/{lang}/LC_MESSAGES/{country_code}.po

    Returns a dict whose keys are the country_codes and whose values are the list of
    languages that are available for that country.

    Beware that countries without any translation won't have their key present in
    the resulting dictionary.

    Returns:
        Dict[str, List[Optional[str]]]: A dictionary mapping country codes to a list
        of languages available for that country.
    """
    """
    module_dir = os.path.dirname(os.path.abspath(holidays.__file__))
    locale_dir = os.path.join(module_dir, 'locale')
    country_codes = {}
    for root, _, files in os.walk(locale_dir):
        for file in files:
            if file.endswith('.po'):
                country_code = re.sub(r'(.*)\.po', r'\1', file)
                lang = os.path.basename(os.path.dirname(root))
                country_codes.setdefault(country_code, []).append(lang)
    return dict(sorted(country_codes.items()))
    """

    return holidays.utils.list_localized_countries()


def get_language_name(code: str) -> str:
    """
    Get the name of a language and, if applicable, the name of the country,
    based on the provided language code.

    Args:
        code (str): A language code, in the format "en_US" or "uk".

    Returns:
        str: The name of the language and, if applicable, the name of the
        country, separated by a space and enclosed in parentheses. If the
        language or country code is invalid, returns "Invalid language or
        country code".
    """
    try:
        language_code, country_code = code.split('_') if '_' in code else (code, None)
        language_name = pycountry.languages.get(alpha_2=language_code).name
        if country_code:
            country_name = pycountry.countries.get(alpha_2=country_code).name
            return f"{language_name} ({country_name})"
        return language_name
    except AttributeError:
        return "Invalid language or country code"


def get_country_name(country_code):
    """
    Get the name of a country from its 2-letter country code.
    """
    country = pycountry.countries.get(alpha_2=country_code)
    if country:
        return country.name
    return country_code


def get_subdiv_name(country_code, subdiv_code):
    """
    Get the name of a subdivision from its country code and subdivision code.
    """
    subdiv = pycountry.subdivisions.get(code=f'{country_code}-{subdiv_code}')
    if subdiv:
        return subdiv.name
    return subdiv_code
