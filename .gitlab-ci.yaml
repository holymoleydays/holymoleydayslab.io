image: nikolaik/python-nodejs:python3.13-nodejs23
stages:
  - checks
  - build
  - deploy

before_script:
  - apt-get update && apt-get upgrade -y
  - apt-get install -y brotli

  - python -m venv .venv
  - source .venv/bin/activate

  - pip install --upgrade pip
  - pip install --upgrade setuptools
  - pip install --upgrade -r requirements.txt

  - npm install -g npm@latest
  - npm install purgecss

generate_html_and_ics_files:
  stage: build
  script:
    - python generate_ics_files.py
    - python generate_html.py
    - mkdir -p build/css
    - ./node_modules/.bin/purgecss -css static/css/*.css --content build/**/*.html --output build/css
    - rm static/css -rf
    - mkdir public
    - mv *.ics public/
    # favicon
    - mv static/*.png build/.
    - mv static/favicon.ico build/.
    - mv static/site.webmanifest build/.
    - mv static/font build/.
    - mv build/* public/.
    # Compress all assets with brotli and gzip
    - find public -type f -not -name '*.br' -not -name '*.gz' -exec brotli -k -f {} \;
    - find public -type f -not -name '*.gz' -not -name '*.br' -exec gzip -kf {} \;
  artifacts:
    paths:
      - public/

pages:
  before_script:
    - echo "Running a before_script to avoid running the default 'before_script'"
  stage: deploy
  script:
    - echo "Deploying to GitLab Pages... The site will be deployed to $CI_PAGES_URL"
  artifacts:
    paths:
      - public/
  only:
    - master
    - auto-upgrade
  dependencies:
    - generate_html_and_ics_files

check_pip_updates:
  stage: checks
  script:
      # Create a fresh virtual environment for update testing (if not we'll be using the one with updated deps)
    - python -m venv .check-venv
    - source .check-venv/bin/activate
    - pip install --upgrade pip
    - pip install --upgrade setuptools
    - pip install -r requirements.txt

    - outdated=$(pip list --outdated | grep holidays || true)

    - deactivate
    - rm -rf .check-venv

    - if [[ ! -z "$outdated" ]]; then echo "$outdated"; exit 1; fi
  only:
    - master
    - dev
    - check-deps

check_pip_safety:
  stage: checks
  script:
    - pip install safety
    - if ! safety check -r requirements.txt | grep -q "No known security vulnerabilities reported."; then echo "Warning! There are known vulnerabilities"; safety check -r requirements.txt; exit 1;fi
  only:
    - master
    - dev
    - check-deps
  allow_failure: true # because safety is a PITA and complains that I'm not paying with an error

check_branch_sync:
  stage: checks
  before_script:
    - echo "Running a before_script to avoid running the default 'before_script'"
  script:
    - |
      main_commit=$(git ls-remote origin refs/heads/master | awk '{print $1}')
      auto_upgrade_commit=$(git ls-remote origin refs/heads/auto-upgrade | awk '{print $1}')
      current_commit=$(git rev-parse HEAD)
      if [[ "$main_commit" == "$auto_upgrade_commit" && "$main_commit" == "$current_commit" ]]; then
        echo "All branches are synchronized (main, auto-upgrade, and check-deps)."
      else
        echo "Branches are not synchronized:"
        echo "master: $main_commit"
        echo "auto-upgrade: $auto_upgrade_commit"
        echo "check-deps: $current_commit"
        exit 1
      fi
  only:
    - check-deps
