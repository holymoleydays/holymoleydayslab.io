"""
This module generates Holy Moly Days! HTML pages using Jinja templates.
"""


import os
from datetime import datetime
import git
from jinja2 import Environment, FileSystemLoader
from utils import (
    get_country_name,
    get_subdiv_name,
    get_language_name,
    list_all_countries_with_translations,
    list_countries_and_subdivs,
)


def init_dirs():
    """
    Create build directories if they don't already exist.
    """
    dirs = ['build', 'build/privacy', 'build/impressum', 'build/about']
    for directory in dirs:
        os.makedirs(directory, exist_ok=True)


def sort_key(string):
    """
    A function that takes a string and returns a key for sorting purposes.

    If the string contains a colon, it splits the string into two parts and
    returns the first part (before the colon) as the sorting key. Otherwise,
    it returns the original string as the sorting key.

    With this, 'Andorra: AD.ics' appears before 'Andorra (Canillo): AD-02.ics', e.g.

    Parameters:
    string (str): The input string to be used as the sorting key.

    Returns:
    str: The sorting key based on the input string.
    """
    parts = string.split(':')
    if len(parts) == 1:
        return parts[0]
    country, rest = parts[0], parts[1]
    return country.lower(), rest.lower()


def generate_html_ics_list():
    """
    Generate the HTML content for the list of ICS files.
    """
    names_and_filenames = []

    countries_with_translations = list_all_countries_with_translations()

    for country, subdiv in list_countries_and_subdivs():
        filename = f'{country}-{subdiv}.ics' if subdiv else f'{country}.ics'

        country_name = get_country_name(country)
        subdiv_name = get_subdiv_name(country, subdiv)
        complete_name = f'{country_name} ({subdiv_name})' if subdiv_name else f'{country_name}'

        names_and_filenames.append((complete_name, filename))

        if country in countries_with_translations:
            for lang in countries_with_translations[country]:
                language_name = get_language_name(lang)
                complete_name_with_lang = (
                    f'{complete_name} ({language_name})'
                    if subdiv_name
                    else f'{complete_name} ({language_name})'
                )
                filename_with_lang = f'{country}-{subdiv}-{lang}.ics' if subdiv else f'{country}-{lang}.ics'

                names_and_filenames.append((complete_name_with_lang, filename_with_lang))

    sorted_names_and_filenames = sorted(names_and_filenames, key=lambda x: sort_key(x[0]))
    return ''.join(
        f'{name}: <a href="webcal://holymolydays.gitlab.io/{filename}">subscribe</a> (or <a href="{filename}" type="text/calendar">download</a>)<br/>\n'
        for name, filename in sorted_names_and_filenames
    )


def render_jinja_templates():
    """
    Render the Jinja templates and generate HTML pages.
    """
    # Set up the Jinja environment
    env = Environment(loader=FileSystemLoader('templates'))

    data = {
        'app': 'Holy Moly Days!',
        'address': os.environ.get('ADDRESS'),
        'email': os.environ.get('EMAIL'),
        'phone': os.environ.get('PHONE'),
        'ics_list_content': generate_html_ics_list(),
        'now': datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
        'commit_sha': git.Repo().head.object.hexsha,
        #'pipeline_url': os.environ.get('CI_JOB_URL', '#'),
        'pipeline_id': os.environ.get('CI_PIPELINE_ID', '#'),
        'pipeline_url': os.environ.get('CI_PIPELINE_URL', '#'),
    }

    template = env.get_template('index.html')
    html_output = template.render(data)
    with open('build/index.html', 'w') as file:
        file.write(html_output)

    template = env.get_template('privacy.html')
    html_output = template.render(data)
    with open('build/privacy/index.html', 'w') as file:
        file.write(html_output)

    template = env.get_template('impressum.html')
    html_output = template.render(data)
    with open('build/impressum/index.html', 'w') as file:
        file.write(html_output)

    template = env.get_template('about.html')
    html_output = template.render(data)
    with open('build/about/index.html', 'w') as file:
        file.write(html_output)


if __name__ == "__main__":
    init_dirs()
    render_jinja_templates()
